AutoMONAI
==============================

Automatic optimization of convolutional neural networks from MONAI for medical image segmentation using automatic machine learning (AutoML).

# Requirements
- Python 3.7


# Installation
- Install via pip
``` bash
$ python -m pip install git+https://gitlab.com/radiology/radiomics/automonai
```

# Authors
- [Martijn P. A. Starmans](https://scholar.google.com/citations?user=rFhvAf8AAAAJ&hl=nl) : [m.starmans@erasmusmc.nl] (mailto:m.starmans@erasmusmc.nl)
- [Karin van Garderen](https://scholar.google.com/citations?user=M39ji4sAAAAJ&hl=nl&oi=ao)
- Teun Tanis
- Coen van Gruijthuijsen
- [Stefan Klein](http://bigr.nl/people/StefanKlein/)
