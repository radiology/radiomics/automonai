"""
**Author** : Martijn P. A. Starmans

**Institution** : AutoMONAI

**Position** : Erasmus Medical Center, Rotterdam, the Netherlands

**Contact** : m.starmans@erasmusmc.nl

**Date** : 2020-11-18

**Project** : AutoMONAI

** AutoMONAI setup file**

"""
import os
import sys
from subprocess import check_output
from pathlib import Path

from setuptools import find_packages, setup


# Main path
SRC_ROOT = Path(
    __file__
).absolute().parent

# Get long description
try:
    readme_path = SRC_ROOT / 'README.md'
    with readme_path.open('r') as readme_handler:
        long_description = readme_handler.read()
except:
    print('[WARNING] Cannot read README.md, replacing long description with "Error".')
    long_description = 'Error'


# Get version
with open(os.path.join(os.path.dirname(__file__),
                       'version')) as version_file:
    version = version_file.read().strip()

# Get requirements
try:
    requirements_path = SRC_ROOT / 'requirements.txt'
    with requirements_path.open('r') as requirements_handler:
        requirements = [
            dependency
            for dependency in requirements_handler.readlines()
            if 'AutoMONAI' not in dependency
        ]
except:
    requirements = []

setup(
    name='AutoMONAI',
    author='Martijn P. A. Starmans, Karin van Garderen',
    version=version,
    packages=find_packages(),
    description='Automatic optization of convolutional neural networks from MONAI for medical image segmentation using automatic machine learning (AutoML).',
    long_description=long_description,
    install_requires=requirements,
    author_email='m.starmans@erasmusmc.nl',
    license='Apache License, Version 2.0',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Healthcare Industry',
        'Intended Audience :: Information Technology',
        'Intended Audience :: Education',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: System :: Distributed Computing',
        'Topic :: System :: Logging',
        'Topic :: Utilities',
    ],
    python_requires=">=3.6",
)
