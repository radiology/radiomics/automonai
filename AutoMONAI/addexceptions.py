#!/usr/bin/env python

# Copyright 2020 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module contains all AutoMONAI-related Exceptions
"""

# import inspect
# import os
# import textwrap

# pylint: disable=too-many-ancestors
# Because fo inheriting from FastrError and a common exception causes this
# exception, even though this behaviour is desired


class AutoMONAIError(Exception):
    """
    This is the base class for all AutoMONAI related exceptions. Catching this
    class of exceptions should ensure a proper execution of AutoMONAI.
    """
    pass


class AutoMONAINotImplementedError(AutoMONAIError, NotImplementedError):
    """
    This function/method has not been implemented on purpose (e.g. should be
    overwritten in a sub-class)
    """
    pass


class AutoMONAIIOError(AutoMONAIError, IOError):
    """
    IOError in AutoMONAI
    """
    pass


class AutoMONAITypeError(AutoMONAIError, TypeError):
    """
    TypeError in the AutoMONAI system
    """
    pass


class AutoMONAIValueError(AutoMONAIError, ValueError):
    """
    ValueError in the AutoMONAI system
    """
    pass


class AutoMONAIKeyError(AutoMONAIError, KeyError):
    """
    KeyError in the AutoMONAI system
    """
    pass


class AutoMONAIAssertionError(AutoMONAIError, AssertionError):
    """
    AssertionError in the AutoMONAI system
    """
    pass


class AutoMONAIIndexError(AutoMONAIError, IndexError):
    """
    IndexError in the AutoMONAI system
    """
    pass
