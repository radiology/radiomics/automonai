Developer documentation
=======================

Documentation
--------------
Documentation is located in the AutoMONAI/doc folder within the
repository and build using `readthedocs <https://readthedocs.org/projects/automonai/>`_.
When adding static documentation files, e.g. non-auto generated,
feel free to change the files in the documentation folder accordingly.

Testing
--------
Unit testing for this repository is done using
`Gitlab pipelines <https://gitlab.com/radiology/radiomics/automonai/-/pipelines/>`_. 
Please add your tests to the tests folder following pytest formatting.
