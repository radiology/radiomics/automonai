"""
**Author** : Martijn P. A. Starmans

**Institution** : AutoMONAI

**Position** : Erasmus Medical Center, Rotterdam, the Netherlands

**Contact** : m.starmans@erasmusmc.nl

**Date** : 2020-11-18

**Project** : AutoMONAI

**Vanilla dummy test**

"""


def test_print() -> None:
    """Test the sum function."""
    print('This is a simple dummy just to be able to run pytest.')
